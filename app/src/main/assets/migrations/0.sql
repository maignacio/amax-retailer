INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('CallTxt Promos', '2', null, 'icons_0022_calltxt', 0);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Surf Promos', '3', null, 'icons_0015_surfpromos', 0);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('IDD Promos', '4', null, 'icons_0014_idd_promos', 0);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Globe Promos', '2*1', '2', 'icons_0016_globepromos', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('TM Promos', '2*2', '2', 'icons_0022_tmpromo', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Nationwide', '2*1*MOB*1', '2*1', 'icons_0021_nationwide', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Baguio', '2*1*MOB*2', '2*1', 'icons_0020_baguio', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Batangas', '2*1*MOB*3', '2*1', 'icons_0019_batangas', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Bicol', '2*1*MOB*4', '2*1', 'icons_0018_bicol', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Laguna', '2*1*MOB*5', '2*1', 'icons_0017_laguna', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Marinduque', '2*1*MOB*6', '2*1', 'icons_0016_marinduque', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Pangasinan', '2*1*MOB*7', '2*1', 'icons_0015_pangasinan', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Quezon', '2*1*MOB*8', '2*1', 'icons_0014_quezon', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI20', '2*1*MOB*1*1', '2*1*MOB*1', 'icons_0013_gounli20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI25', '2*1*MOB*1*2', '2*1*MOB*1', 'icons_0012_gounli25', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI30', '2*1*MOB*1*3', '2*1*MOB*1', 'icons_0001_gounli30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI50', '2*1*MOB*1*4', '2*1*MOB*1', 'icons_0011_gounli50', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLIALL20', '2*1*MOB*2*1', '2*1*MOB*2', 'icons_0010_gounliall20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI20', '2*1*MOB*2*2', '2*1*MOB*2', 'icons_0013_gounli20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI25', '2*1*MOB*2*3', '2*1*MOB*2', 'icons_0012_gounli25', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI30', '2*1*MOB*2*4', '2*1*MOB*2', 'icons_0001_gounli30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI50', '2*1*MOB*2*5', '2*1*MOB*2', 'icons_0011_gounli50', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLIALL20', '2*1*MOB*3*1', '2*1*MOB*3', 'icons_0010_gounliall20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI20', '2*1*MOB*3*2', '2*1*MOB*3', 'icons_0013_gounli20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI25', '2*1*MOB*3*3', '2*1*MOB*3', 'icons_0012_gounli25', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI30', '2*1*MOB*3*4', '2*1*MOB*3', 'icons_0001_gounli30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI50', '2*1*MOB*3*5', '2*1*MOB*3', 'icons_0011_gounli50', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOALL20', '2*1*MOB*4*1', '2*1*MOB*4', 'icons_0001_goall20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('TXT20', '2*1*MOB*4*2', '2*1*MOB*4', 'icons_0008_txt20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI20', '2*1*MOB*4*3', '2*1*MOB*4', 'icons_0013_gounli20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI25', '2*1*MOB*4*4', '2*1*MOB*4', 'icons_0012_gounli25', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI30', '2*1*MOB*4*5', '2*1*MOB*4', 'icons_0001_gounli30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOCALL30', '2*1*MOB*4*6', '2*1*MOB*4', 'icons_0002_gocall30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI50', '2*1*MOB*4*7', '2*1*MOB*4', 'icons_0011_gounli50', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLIALL20', '2*1*MOB*5*1', '2*1*MOB*5', 'icons_0010_gounliall20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI20', '2*1*MOB*5*2', '2*1*MOB*5', 'icons_0013_gounli20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI25', '2*1*MOB*5*3', '2*1*MOB*5', 'icons_0012_gounli25', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI30', '2*1*MOB*5*4', '2*1*MOB*5', 'icons_0001_gounli30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI50', '2*1*MOB*5*5', '2*1*MOB*5', 'icons_0011_gounli50', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOALL20', '2*1*MOB*6*1', '2*1*MOB*6', 'icons_0001_goall20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('TXT20', '2*1*MOB*6*2', '2*1*MOB*6', 'icons_0008_txt20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI20', '2*1*MOB*6*3', '2*1*MOB*6', 'icons_0013_gounli20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI25', '2*1*MOB*6*4', '2*1*MOB*6', 'icons_0012_gounli25', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI30', '2*1*MOB*6*5', '2*1*MOB*6', 'icons_0001_gounli30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOCALL30', '2*1*MOB*6*6', '2*1*MOB*6', 'icons_0002_gocall30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI50', '2*1*MOB*6*7', '2*1*MOB*6', 'icons_0011_gounli50', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLIALL20', '2*1*MOB*7*1', '2*1*MOB*7', 'icons_0010_gounliall20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI20', '2*1*MOB*7*2', '2*1*MOB*7', 'icons_0013_gounli20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI25', '2*1*MOB*7*3', '2*1*MOB*7', 'icons_0012_gounli25', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI30', '2*1*MOB*7*4', '2*1*MOB*7', 'icons_0001_gounli30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI50', '2*1*MOB*7*5', '2*1*MOB*7', 'icons_0011_gounli50', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOALL20', '2*1*MOB*8*1', '2*1*MOB*8', 'icons_0001_goall20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('TXT20', '2*1*MOB*8*2', '2*1*MOB*8', 'icons_0008_txt20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI20', '2*1*MOB*8*3', '2*1*MOB*8', 'icons_0013_gounli20', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI25', '2*1*MOB*8*4', '2*1*MOB*8', 'icons_0012_gounli25', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI30', '2*1*MOB*8*5', '2*1*MOB*8', 'icons_0001_gounli30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOCALL30', '2*1*MOB*8*6', '2*1*MOB*8', 'icons_0002_gocall30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOUNLI50', '2*1*MOB*8*7', '2*1*MOB*8', 'icons_0011_gounli50', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Nationwide', '2*2*MOB*1', '2*2', 'icons_0021_nationwide', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Bicol', '2*2*MOB*2', '2*2', 'icons_0018_bicol', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Marinduque', '2*2*MOB*3', '2*2', 'icons_0016_marinduque', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Quezon', '2*2*MOB*4', '2*2', 'icons_0014_quezon', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('TXT10', '2*2*MOB*1*1', '2*2*MOB*1', 'icons_0005_txt10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('UA10', '2*2*MOB*1*2', '2*2*MOB*1', 'icons_0007_unliallnet10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO10', '2*2*MOB*1*3', '2*2*MOB*1', 'icons_0010_combo10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO15', '2*2*MOB*1*4', '2*2*MOB*1', 'icons_0009_combo15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('UNLICALL15', '2*2*MOB*1*5', '2*2*MOB*1', 'icons_0006_unlicall15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO20', '2*2*MOB*1*6', '2*2*MOB*1', 'icons_0007_combo20', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('ASTIGTXT30', '2*2*MOB*1*7', '2*2*MOB*1', 'icons_0013_astigtxt30', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('UT10', '2*2*MOB*2*1', '2*2*MOB*2', 'icons_0006_ut10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO10', '2*2*MOB*2*2', '2*2*MOB*2', 'icons_0010_combo10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO15', '2*2*MOB*2*3', '2*2*MOB*2', 'icons_0009_combo15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('UNLICALL15', '2*2*MOB*2*4', '2*2*MOB*2', 'icons_0006_unlicall15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO20', '2*2*MOB*2*5', '2*2*MOB*2', 'icons_0007_combo20', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('ASTIGTXT30', '2*2*MOB*2*6', '2*2*MOB*2', 'icons_0013_astigtxt30', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('UT10', '2*2*MOB*3*1', '2*2*MOB*3', 'icons_0006_ut10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO10', '2*2*MOB*3*2', '2*2*MOB*3', 'icons_0010_combo10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO15', '2*2*MOB*3*3', '2*2*MOB*3', 'icons_0009_combo15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('UNLICALL15', '2*2*MOB*3*4', '2*2*MOB*3', 'icons_0006_unlicall15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO20', '2*2*MOB*3*5', '2*2*MOB*3', 'icons_0007_combo20', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('ASTIGTXT30', '2*2*MOB*3*6', '2*2*MOB*3', 'icons_0013_astigtxt30', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('UT10', '2*2*MOB*4*1', '2*2*MOB*4', 'icons_0006_ut10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO10', '2*2*MOB*4*2', '2*2*MOB*4', 'icons_0010_combo10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO15', '2*2*MOB*4*3', '2*2*MOB*4', 'icons_0009_combo15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('UNLICALL15', '2*2*MOB*4*4', '2*2*MOB*4', 'icons_0006_unlicall15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('COMBO20', '2*2*MOB*4*5', '2*2*MOB*4', 'icons_0007_combo20', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('ASTIGTXT30', '2*2*MOB*4*6', '2*2*MOB*4', 'icons_0013_astigtxt30', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Globe Mobile Internet Promos', '3*1', '3', 'icons_0016_globepromos', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('TM Mobile Internet Promos', '3*2', '3', 'icons_0022_tmpromo', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOSURF10', '3*1*1', '3*1', 'icons_0005_gosurf10', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOSURF15', '3*1*2', '3*1', 'icons_0004_gosurf15', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOSURF50', '3*1*3', '3*1', 'icons_0003_gosurf50', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOSURF99', '3*1*4', '3*1', 'icons_0002_gosurf99', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOSURF10', '3*2*1', '3*2', 'icons_0005_gosurf10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOSURF15', '3*2*2', '3*2', 'icons_0004_gosurf15', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOSURF50', '3*2*3', '3*2', 'icons_0003_gosurf50', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GOSURF99', '3*2*4', '3*2', 'icons_0002_gosurf99', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('ASTIGFB10', '3*2*5', '3*2', 'icons_0040_astigfb10', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('ASTIGFB15', '3*2*6', '3*2', 'icons_0001_astigfb15', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('Globe Promos', '4*1', '4', 'icons_0016_globepromos', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('TM Promos', '4*2', '4', 'icons_0022_tmpromo', 2);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GoTipIDD30', '4*1*1', '4*1', 'icons_0042_tipidd30', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GoTipIDD50', '4*1*2', '4*1', 'icons_0027_gotipidd50', 1);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GoTipIDD100', '4*1*3', '4*1', 'icons_0026_gotipidd100', 1);

INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('ASTIGITXT20', '4*2*1', '4*2', 'icons_0000_astigtxt20', 2);
INSERT INTO Promo (title, ussd_string, parent_ussd_string, icon, type) VALUES ('GoTipIDD30', '4*2*2', '4*2', 'icons_0042_tipidd30', 2);