package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.OnItemClick;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.AppData;
import ph.com.globe.globeamaxretailer.util.AlarmManagerHelper;

public class LauncherActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(android.R.id.list)
    ListView mListView;

    private ListAdapter mAdapter;

    public static void start(Activity activity) {
        dial(activity, null);
    }

    public static void dial(Activity activity, String uriString) {
        Intent intent = new Intent(activity, LauncherActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, uriString);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        setSupportActionBar(mToolbar);

        View header = View.inflate(this, R.layout.header_launcher, null);

        mAdapter = new ListAdapter(this);
        mListView.addHeaderView(header);
        mListView.setAdapter(mAdapter);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String uriString = extras.getString(Intent.EXTRA_TEXT);

            if (!TextUtils.isEmpty(uriString))
                dial(uriString);
        }

        AlarmManagerHelper.setAlarm(LauncherActivity.this);

        if (!AppData.isUserVerified(getApplicationContext())) {
            LoginActivity.start(this);
            finish();
            return;
        }

        try {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String simSerial = telephonyManager.getSimSerialNumber();

            String storedSimSerial = AppData.getSimSerialNumber(getApplicationContext());

            if ((!TextUtils.isEmpty(simSerial) && TextUtils.isEmpty(storedSimSerial)) || (!TextUtils.isEmpty(simSerial) && !simSerial.equals(storedSimSerial))) {
                AppData.reset(getApplicationContext());

                LoginActivity.start(this);
                finish();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnItemClick(android.R.id.list)
    void select(int position) {
        switch (position) {
            case 1:
                LoadActivity.start(LauncherActivity.this);
                break;
            case 2:
                PromoActivity.start(LauncherActivity.this);
                break;
            case 3:
                showAlertDialog(R.string.error_launcher_feature_unavailable);
                break;
            case 4:
                HelpActivity.start(LauncherActivity.this);
                break;
            case 5:
                SchedulerActivity.start(LauncherActivity.this);
                break;
        }
    }

    private void dial(String uriString) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uriString));

        startActivity(intent);
    }


    private class ListAdapter extends BaseAdapter {
        private Context mContext;
        private String[] mValues;
        private TypedArray mIconDrawables;

        public ListAdapter(Context context) {
            super();

            mContext = context;
            mValues = mContext.getResources().getStringArray(R.array.values_launcher);
            mIconDrawables = mContext.getResources().obtainTypedArray(R.array.icons_launcher);
        }

        @Override
        public int getCount() {
            return mValues.length;
        }

        @Override
        public String getItem(int position) {
            return mValues[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list, null);
            }

            TextView textView = (TextView) convertView;
            textView.setText(getItem(position));
            textView.setCompoundDrawablesWithIntrinsicBounds(mIconDrawables.getResourceId(position, 0), 0, (position != 2) ? R.drawable.icons_0001_arrow : 0, 0);
            return convertView;
        }
    }
}
