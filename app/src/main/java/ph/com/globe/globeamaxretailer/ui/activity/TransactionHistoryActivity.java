package ph.com.globe.globeamaxretailer.ui.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.Bind;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.cache.ContactsCache;
import ph.com.globe.globeamaxretailer.data.TransactionLog;
import ph.com.globe.globeamaxretailer.util.DateTimeFormatHelper;
import ph.com.globe.globeamaxretailer.util.ViewHolder;

public class TransactionHistoryActivity extends BaseActivity {
    @Bind(android.R.id.list)
    ListView mListView;
    @Bind(R.id.btn_email)
    Button mBtnEmail;

    private ExecutorService mThreadPool = Executors.newSingleThreadExecutor();
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private ListAdapter mAdapter;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, TransactionHistoryActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus.getDefault().register(this);

        setContentView(R.layout.activity_transaction_history);

        enableBackButton();

        mAdapter = new ListAdapter(this);

        View empty = getLayoutInflater().inflate(R.layout.view_transaction_history_empty, null);
        addContentView(empty, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mListView.setEmptyView(empty);
        mListView.setAdapter(mAdapter);

        update();

        /*TransactionLog log  = new TransactionLog();
        log.recipient = "09171234567";
        log.refNumber = "1234567";
        log.denomination = "50";
        log.retailerValue = "45";
        log.datetime = Calendar.getInstance();
        log.datetime.setTimeInMillis(System.currentTimeMillis());
        log.save();

        log  = new TransactionLog();
        log.recipient = "09176880132";
        log.refNumber = "asdfjkl;";
        log.denomination = "500";
        log.retailerValue = "450";
        log.datetime = Calendar.getInstance();
        log.datetime.setTimeInMillis(System.currentTimeMillis());
        log.save();*/
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }

    @OnClick(R.id.btn_email)
    void sendEmail() {
        ProcessLogsToCSVActivity.start(TransactionHistoryActivity.this);
    }

    private void update() {
        mThreadPool.submit(new Runnable() {
            @Override
            public void run() {
                final Cursor cursor = TransactionLog.getAllTransactionLogs();

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mBtnEmail.setVisibility((cursor.getCount() > 0) ? View.VISIBLE : View.GONE);
                        mAdapter.swapCursor(cursor);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    public void onEventMainThread(ContactsCache.Event event) {
        mAdapter.notifyDataSetChanged();
    }

    public void onEventMainThread(TransactionLog.Event event) {
        update();
    }

    private class ListAdapter extends CursorAdapter {
        private LayoutInflater mInflater;

        public ListAdapter(Context context) {
            super(context, null, true);

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return mInflater.inflate(R.layout.item_transaction_history, null);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TransactionLog log = new TransactionLog();
            log.loadFromCursor(cursor);

            TextView title = ViewHolder.get(view, R.id.title);
            TextView amount = ViewHolder.get(view, R.id.amount);
            TextView time = ViewHolder.get(view, R.id.time);
            TextView refCode = ViewHolder.get(view, R.id.ref_code);

            String recipient = ContactsCache.getEntry(log.recipient);

            if (TextUtils.isEmpty(recipient)) {
                ContactsCache.enqueueContactPhotoRequest(context, log.recipient);
                title.setText(log.recipient);
            }
            else {
                title.setText(recipient);
            }

            amount.setText(getString(R.string.peso) + log.denomination + " (" + getString(R.string.peso) + log.retailerValue + ")");
            time.setText(DateTimeFormatHelper.formatShortDateTime(log.datetime.getTimeInMillis()));
            refCode.setText("Ref: " + log.refNumber);
        }
    }
}