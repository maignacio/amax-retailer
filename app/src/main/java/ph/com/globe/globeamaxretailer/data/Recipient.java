package ph.com.globe.globeamaxretailer.data;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;

@Table(name = "Recipients", id = "_id")
public class Recipient extends Model {
    @Column(name = "number", notNull = true)
    public String number;

    @Column(name = "schedule", notNull = true)
    public Schedule schedule;

    public String name;

    public static void deleteRecipientsOfSchedule(long scheduleId) {
        new Delete().from(Recipient.class).where("schedule = " + scheduleId).execute();
    }
}
