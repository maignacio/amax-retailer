package ph.com.globe.globeamaxretailer.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import ph.com.globe.globeamaxretailer.ui.activity.LauncherActivity;

public class UssdHelper {
    private static final String USSD_LOAD_SHORTCODE = "*100*";
    private static final String USSD_LOAD_TOKEN = "*";
    private static final String USSD_LOAD_END = "*1" + Uri.encode("#");

    public static void sendLoad(Activity activity, String recipient, String denomination) {
        StringBuilder builder = new StringBuilder();
        builder.append("1");
        builder.append(USSD_LOAD_TOKEN);
        builder.append(recipient);
        builder.append(USSD_LOAD_TOKEN);
        builder.append(denomination);

        call(activity, builder.toString());
    }

    public static void sendLoadViaDirectUssd(Activity activity, String recipient, String denomination) {
        StringBuilder builder = new StringBuilder();
        builder.append("1");
        builder.append(USSD_LOAD_TOKEN);
        builder.append(recipient);
        builder.append(USSD_LOAD_TOKEN);
        builder.append(denomination);

        String uri = "tel:" + USSD_LOAD_SHORTCODE + builder.toString().trim() + USSD_LOAD_END;

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));

        activity.startActivity(intent);
    }

    public static void sendPromo(Activity activity, String ussdString) {
        call(activity, ussdString);
    }

    private static void call(Activity activity, String ussdString) {
        String uri = "tel:" + USSD_LOAD_SHORTCODE + ussdString.trim() + USSD_LOAD_END;

        LauncherActivity.dial(activity, uri);
    }
}
