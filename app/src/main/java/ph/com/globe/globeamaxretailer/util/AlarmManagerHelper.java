package ph.com.globe.globeamaxretailer.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import ph.com.globe.globeamaxretailer.data.Transaction;

public class AlarmManagerHelper {

    public static final String INTENT_ACTION = "ph.com.globe.globeamaxretailer.util.AlarmManagerHelper.INTENT_ACTION";
    public static final String INTENT_EXTRA = "ph.com.globe.globeamaxretailer.util.AlarmManagerHelper.INTENT_EXTRA";
    private static final int PENDING_INTENT_ID = 101011;

    /**
     * Create an alarm for the next transaction
     *
     * @param context Application context
     */
    public static void setAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        long timeInMillis = Transaction.getTimeInMillisOfNextPendingTransaction();

        Intent intent = new Intent();
        intent.setAction(INTENT_ACTION);
        intent.putExtra(INTENT_EXTRA, timeInMillis);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, PENDING_INTENT_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (timeInMillis < System.currentTimeMillis()) {
            context.sendBroadcast(intent);
        } else {
            // Set alarm
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
            } else {
                alarmManager.set(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
            }
        }
    }
}
