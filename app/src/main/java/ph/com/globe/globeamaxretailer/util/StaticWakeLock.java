package ph.com.globe.globeamaxretailer.util;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;

public class StaticWakeLock {
    private static PowerManager.WakeLock wl = null;

    public static void lockOn(Context context) {
        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km.newKeyguardLock("PROCESS TRANSACTIONS");
        kl.disableKeyguard();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (wl == null)
            wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "PROCESS TRANSACTIONS");
        wl.acquire();
    }

    public static void lockOff(Context context) {
        try {
            if (wl != null)
                wl.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}