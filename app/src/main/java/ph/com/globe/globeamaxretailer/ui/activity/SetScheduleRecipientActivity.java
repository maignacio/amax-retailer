package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.widget.EditText;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.cache.RecipientsCache;
import ph.com.globe.globeamaxretailer.data.Recipient;
import ph.com.globe.globeamaxretailer.ui.widget.ContactPickerView;
import ph.com.globe.globeamaxretailer.util.MobileNumberUtil;

public class SetScheduleRecipientActivity extends BaseSchedulerActivity {
    @Bind(R.id.input_title)
    EditText mInputTitle;
    @Bind(R.id.input_recipients)
    ContactPickerView mInputRecipients;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SetScheduleRecipientActivity.class);
        activity.startActivity(intent);
    }

    public static void start(Activity activity, long scheduleId) {
        Intent intent = new Intent(activity, SetScheduleRecipientActivity.class);
        intent.putExtra(Intent.EXTRA_REFERRER, scheduleId);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_schedule_recipient);

        enableBackButton();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri contactUri = data.getData();
            if (null == contactUri) return;

            Cursor cursor = getContentResolver().query(contactUri, new String[]{ContactsContract.CommonDataKinds.Phone.DATA}, null, null, null);
            if (null == cursor) return;
            try {
                cursor.moveToFirst();
                String number = MobileNumberUtil.formatNumber(cursor.getString(0));

                if (MobileNumberUtil.isValidNumber(number)) {
                    mInputRecipients.shouldAdd(number, false);
                } else {
                    showAlertDialog(R.string.error_set_schedule_recipients_invalid);
                }
            } finally {
                cursor.close();
            }
        }
    }

    @Override
    void updateInBackground() {
        final List<Recipient> recipientList = RecipientsCache.getEntry(mSchedule.getId().intValue());

        if (recipientList != null && !recipientList.isEmpty()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mInputTitle.setText(mSchedule.name);

                    int count = recipientList.size();
                    for (int i = 0; i < count; i++) {
                        mInputRecipients.shouldAdd(recipientList.get(i).number, false);
                    }
                }
            });
        } else {
            RecipientsCache.enqueueContactPhotoRequest(SetScheduleRecipientActivity.this, mSchedule.getId().intValue());
        }
    }

    @OnClick(R.id.btn_contacts)
    void pickContact() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(intent, 100);
        } catch (Exception e) {
            e.printStackTrace();
            showAlertDialog(R.string.error_send_load_no_phonebook);
        }
    }

    @OnClick(R.id.btn_submit)
    void submit() {
        String title = mInputTitle.getText().toString();
        final List<String> numbers = mInputRecipients.getContacts();

        if (numbers.isEmpty()) {
            showAlertDialog(R.string.error_set_schedule_recipients_invalid);
            return;
        }

        if (TextUtils.isEmpty(title)) {
            title = null;
        }

        SetScheduleDenominationActivity.start(SetScheduleRecipientActivity.this, mScheduleId, title, numbers);
    }

    public void onEventMainThread(RecipientsCache.Event event) {
        update();
    }
}
