package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;

import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.service.CSVWriterService;
import ph.com.globe.globeamaxretailer.util.DateTimeFormatHelper;

public class ProcessLogsToCSVActivity extends BaseActivity {

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ProcessLogsToCSVActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_transactions);

        getSupportActionBar().hide();

        EventBus.getDefault().register(this);

        CSVWriterService.start(this);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }

    public void onEventMainThread(CSVWriterService.Event event) {
        CSVWriterService.Event.Type type = event.type;

        switch (type) {
            case Success:
                String fileName = event.fileName;

                if (!TextUtils.isEmpty(fileName)) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("message/rfc822");
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Autoload MAX Transaction Logs as of " + DateTimeFormatHelper.formatShortDateTime(System.currentTimeMillis()));
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + fileName));
                    startActivity(Intent.createChooser(intent, "Sending email..."));
                }

                finish();
                break;
            case Fail:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Error creating file.");
                builder.setCancelable(false);
                builder.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                AppCompatDialog dialog = builder.create();
                dialog.show();
                break;
        }
    }
}