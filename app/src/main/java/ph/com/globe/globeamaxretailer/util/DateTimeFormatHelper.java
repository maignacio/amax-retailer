package ph.com.globe.globeamaxretailer.util;

import java.text.SimpleDateFormat;

public class DateTimeFormatHelper {
    private static SimpleDateFormat mShortFormat = new SimpleDateFormat("MM/dd/yyyy h:mm a");
    private static SimpleDateFormat mLongFormat = new SimpleDateFormat("MMMM d, yyyy h:mm a");
    private static SimpleDateFormat mDateOnlyFormat = new SimpleDateFormat("MM/dd/yyyy");
    private static SimpleDateFormat mTimeOnlyFormat = new SimpleDateFormat("h:mm a");

    public static String formatShortDateTime(long timeInMillis) {
        return mShortFormat.format(timeInMillis);
    }

    public static String formatLongDateTime(long timeInMillis) {
        return mLongFormat.format(timeInMillis);
    }

    public static String formatDate(long timeInMillis) {
        return mDateOnlyFormat.format(timeInMillis);
    }

    public static String formatTime(long timeInMillis) {
        return mTimeOnlyFormat.format(timeInMillis);
    }

    public static SimpleDateFormat getTimeFormat() {
        return mTimeOnlyFormat;
    }
}
