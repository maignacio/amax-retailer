package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.util.MobileNumberUtil;
import ph.com.globe.globeamaxretailer.util.UssdHelper;

public class LoadActivity extends BaseActivity {
    @Bind(R.id.input_mobile_number)
    EditText mInputMobileNumber;
    @Bind(R.id.input_amount)
    EditText mInputAmount;
    @Bind(R.id.denom_container)
    View mDenomContainer;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, LoadActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);

        enableBackButton();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri contactUri = data.getData();
            if (null == contactUri) return;

            Cursor cursor = getContentResolver().query(contactUri, new String[]{ContactsContract.CommonDataKinds.Phone.DATA}, null, null, null);
            if (null == cursor) return;
            try {
                cursor.moveToFirst();
                String number = cursor.getString(0);

                mInputMobileNumber.setText(MobileNumberUtil.formatNumber(number));
            } finally {
                cursor.close();
            }
        }
    }

    @OnFocusChange(R.id.input_amount)
    void onFocusChanged(boolean focused) {
        if (focused)
            mDenomContainer.setBackgroundResource(R.drawable.bg_edit_text_focused);

        else
            mDenomContainer.setBackgroundResource(R.drawable.bg_edit_text_normal);

    }

    @OnClick(R.id.btn_contacts)
    void pickContact() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(intent, 100);
        } catch (Exception e) {
            e.printStackTrace();
            showAlertDialog(R.string.error_send_load_no_phonebook);
        }
    }

    @OnClick(R.id.btn_send_load)
    void submit() {
        final String mobileNumber = MobileNumberUtil.formatNumber(mInputMobileNumber.getText().toString());
        final String amount = mInputAmount.getText().toString().replaceAll("[^\\d]", "");

        if (TextUtils.isEmpty(mobileNumber) || TextUtils.isEmpty(amount)) {
            showAlertDialog(R.string.error_send_load_missing);
            return;
        }

        boolean isGlobe = MobileNumberUtil.isGlobeNumber(mobileNumber);
        boolean isTM = MobileNumberUtil.isTMNumber(mobileNumber);

        if (!isGlobe && !isTM) {
            showAlertDialog(R.string.error_send_load_mobile_invalid);
            return;
        }

        if (!isDenominationValid(amount, isTM)) {
            showAlertDialog(R.string.error_send_load_denomination_invalid);
            return;
        }

        String successMessage = getString(R.string.success_send_load_message)
                .replaceAll(getString(R.string.success_send_load_message_key_mobile), mobileNumber)
                .replaceAll(getString(R.string.success_send_load_message_key_denomination), amount);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.success_send_load_title);
        builder.setMessage(Html.fromHtml(successMessage));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                UssdHelper.sendLoad(LoadActivity.this, mobileNumber, amount);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);

        AppCompatDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick({R.id.btn_denom_1, R.id.btn_denom_2, R.id.btn_denom_3, R.id.btn_denom_4})
    void selectDenomination(Button button) {
        mInputAmount.setText(button.getText().toString().substring(1));
    }

    private boolean isDenominationValid(String amount, boolean forTMNumber) {
        try {
            int denomination = Integer.valueOf(amount);

            if (forTMNumber && denomination >= 5 && denomination <= 150) {
                return true;
            }

            if (denomination >= 10 && denomination <= 150) {
                return true;
            }

            switch (denomination) {
                case 350:
                case 450:
                case 550:
                case 600:
                case 700:
                case 900:
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
