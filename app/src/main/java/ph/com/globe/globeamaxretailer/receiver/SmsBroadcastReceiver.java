package ph.com.globe.globeamaxretailer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.AppData;
import ph.com.globe.globeamaxretailer.data.TransactionLog;

public class SmsBroadcastReceiver extends BroadcastReceiver {
    private static final String REGEX_CURRENCY = "(((\\d{1,3},)+\\d{3})|\\d+)\\.\\d{2}";
    private static final String REGEX_SUCCESS_MESSAGE = "(\\d{1,2})/(\\d{1,2})/(\\d{4}) (\\d{1,2}):(\\d{1,2})([aApP][mM]) \\d{11} has loaded (" + REGEX_CURRENCY + ") Load[(][P](" + REGEX_CURRENCY + ")[)] to (\\d{11})[.] New load wallet balance is [P](" + REGEX_CURRENCY + ")[.] Ref:(.*)";

    private static final String REGEX_REGISTRATION_SUCCESS_MESSAGE = "Thank for you downloading the AMAX App. You can now start selling load using this app. No need for internet connection!";

    private final Pattern patternSuccessMessage = Pattern.compile(REGEX_SUCCESS_MESSAGE);
    private final Pattern patternRegistrationSuccessMessage = Pattern.compile(REGEX_REGISTRATION_SUCCESS_MESSAGE);

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            for (int i = 0; i < pdus.length; i++) {

                SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                String sender = currentMessage.getDisplayOriginatingAddress();
                String message = currentMessage.getDisplayMessageBody();

                parseSmsMessage(context, sender, message);
            }
        }
    }

    public void parseSmsMessage(Context context, String sender, String message) {
        if (PhoneNumberUtils.stripSeparators(context.getString(R.string.sms_shortcode)).equals(PhoneNumberUtils.stripSeparators(sender))) {
            Matcher registrationSuccessMessageMatcher = patternRegistrationSuccessMessage.matcher(message);

            if (registrationSuccessMessageMatcher.matches()) {
                try {
                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    String simSerial = telephonyManager.getSimSerialNumber();

                    AppData.setSimSerialNumber(context, simSerial);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                AppData.setUserAsVerified(context);

                return;
            } else {
                EventBus.getDefault().post(new Event(Event.Type.VerificationError).withMessage(message));
            }
        }

        Matcher successMessageMatcher = patternSuccessMessage.matcher(message);

        if (successMessageMatcher.matches()) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Integer.valueOf(successMessageMatcher.group(3)), Integer.valueOf(successMessageMatcher.group(1)) - 1, Integer.valueOf(successMessageMatcher.group(2)));
            calendar.set(Calendar.HOUR, Integer.valueOf(successMessageMatcher.group(4)));
            calendar.set(Calendar.MINUTE, Integer.valueOf(successMessageMatcher.group(5)));
            calendar.set(Calendar.AM_PM, (successMessageMatcher.group(6).equalsIgnoreCase("am")) ? Calendar.AM : Calendar.PM);

            String denomination = successMessageMatcher.group(7);
            String retailerPrice = successMessageMatcher.group(11);

            String recipient = successMessageMatcher.group(15);
            String refNum = successMessageMatcher.group(20);

            TransactionLog.addTransactionLog(calendar, recipient, denomination, retailerPrice, refNum);
        }
    }

    public static class Event {
        private Type mType = Type.VerificationError;
        private String mMessage;
        public Event(Type type) {
            mType = type;
        }

        public Event withMessage(String message) {
            mMessage = message;

            return this;
        }

        public Type getType() {
            return mType;
        }

        public String getMessage() {
            return mMessage;
        }

        public enum Type {
            VerificationError
        }
    }
}
