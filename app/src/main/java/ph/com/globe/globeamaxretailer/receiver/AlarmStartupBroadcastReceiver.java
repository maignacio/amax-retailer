package ph.com.globe.globeamaxretailer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ph.com.globe.globeamaxretailer.util.AlarmManagerHelper;

public class AlarmStartupBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmManagerHelper.setAlarm(context);
    }
}
