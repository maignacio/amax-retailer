package ph.com.globe.globeamaxretailer;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class GlobeAMAXRetailer extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

        ActiveAndroid.initialize(this);
    }
}
