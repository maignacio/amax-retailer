package ph.com.globe.globeamaxretailer.ui.widget;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.Contact;
import ph.com.globe.globeamaxretailer.ui.widget.imageview.transform.CircleTransform;
import ph.com.globe.globeamaxretailer.util.MobileNumberUtil;
import ph.com.globe.globeamaxretailer.util.ViewHolder;

public class ContactPickerView extends RowLayout implements ContactsAutoCompleteTextView.Listener {
    private ContactsAutoCompleteTextView input;
    private Adapter mAdapter;

    private ExecutorService pool = Executors.newSingleThreadExecutor();

    private List<Contact> mContacts = null;

    private String extraSQLClause = null;

    public ContactPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.widget_contact_picker, this);

        mContacts = new ArrayList<Contact>();

        input = (ContactsAutoCompleteTextView) findViewById(R.id.input);
        input.setListener(this);
        input.setDropDownAnchor(this.getId());
        input.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact contact = new Contact();
                Cursor cursor = mAdapter.getCursor();
                cursor.moveToPosition(position);
                contact.loadFromCursor(cursor);

                addChip(contact);
            }
        });

        mAdapter = new Adapter(getContext());
        mAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence str) {
                return Contact.getCursor(MobileNumberUtil.replacePrefixes(str.toString()), extraSQLClause);
            }
        });

        input.setAdapter(mAdapter);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                input.requestFocus();
                showKeyboard(input);
            }
        });
    }

    public List<String> getContacts() {
        ArrayList<String> contacts = new ArrayList<String>();

        int numContacts = mContacts.size();
        for (int i = 0; i < numContacts; i++) {
            contacts.add(mContacts.get(i).number);
        }

        return contacts;
    }

    public void clear() {
        int count = mContacts.size();

        for (int i = 0; i < count; i++) {
            removeChip(0);
        }
    }

    private void addChip(Contact contact) {
        addChip(contact, true);
    }

    private void addChip(Contact contact, boolean fromInputField) {
        mContacts.add(contact);

        ContactChipsView chip = new ContactChipsView(getContext());
        chip.setContact(contact);

        this.addView(chip, mContacts.size() - 1);

        calculateExtraClause();

        if (fromInputField) {
            input.setText(null);
        }
    }

    private void removeChip(int index) {
        if (mContacts.size() < 1) return;

        mContacts.remove(index);
        this.removeViewAt(index);

        calculateExtraClause();
    }

    private void calculateExtraClause() {
        if (mContacts.isEmpty()) {
            extraSQLClause = null;
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append("number NOT IN ('");
            builder.append(mContacts.get(0).number);

            for (int i = 1; i < mContacts.size(); i++) {
                builder.append("', '");
                builder.append(mContacts.get(i).number);
            }

            builder.append("')");

            extraSQLClause = builder.toString();
        }
    }

    private void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void shouldDelete() {
        removeChip(mContacts.size() - 1);
    }

    @Override
    public void shouldAdd(String number) {
        shouldAdd(number, true);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFocusChanged(boolean hasFocus) {
        if (hasFocus)
            setBackgroundResource(R.drawable.bg_edit_text_focused);

        else
            setBackgroundResource(R.drawable.bg_edit_text_normal);
    }

    public void shouldAdd(final String number, final boolean fromInputField) {
        final String msisdn = number;
        final Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                Contact contact = (Contact) msg.obj;
                addChip(contact, fromInputField);
                return true;
            }
        });

        pool.submit(new Runnable() {
            @Override
            public void run() {
                Contact contact = Contact.getContactFromPhone(msisdn);

                if (contact == null) {
                    contact = new Contact();
                    contact.number = msisdn;
                }

                Message msg = Message.obtain();
                msg.obj = contact;

                handler.sendMessage(msg);
            }
        });
    }

    private class Adapter extends CursorAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        private ColorGenerator colorGenerator;

        public Adapter(Context context) {
            super(context, null, true);

            mContext = context;
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            colorGenerator = ColorGenerator.DEFAULT;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View newView = mInflater.inflate(R.layout.item_contacts_autocomplete, null);
            return newView;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            Contact contact = new Contact();
            contact.loadFromCursor(cursor);

            ImageView avatar = ViewHolder.get(view, R.id.avatar);
            TextView name = ViewHolder.get(view, R.id.name);
            TextView number = ViewHolder.get(view, R.id.number);

            TextDrawable drawable = TextDrawable.builder().buildRound(contact.name.toUpperCase().substring(0, 1), getResources().getColor(R.color.dark_blue));

            Picasso.with(getContext()).load(contact.uri).error(drawable).placeholder(drawable).transform(new CircleTransform()).into(avatar);
            name.setText(contact.name);
            number.setText(contact.number);
        }
    }
}
