package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.widget.Button;

import butterknife.Bind;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.AppData;
import ph.com.globe.globeamaxretailer.receiver.SmsBroadcastReceiver;

public class LoginActivity extends BaseActivity {
    private final int SMS_REGISTRATION_TIMEOUT = 60000;
    @Bind(R.id.btn_tc)
    Button btnTermsAndConditions;
    private ProgressDialog mProgress = null;
    private Handler mHandler;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus.getDefault().register(this);

        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();

        mHandler = new Handler(getMainLooper());

        btnTermsAndConditions.setText(Html.fromHtml(getString(R.string.button_tc)));
    }

    @Override
    protected void onDestroy() {
        mHandler.removeCallbacksAndMessages(null);
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }

    @OnClick(R.id.btn_tc)
    void showTermsAndConditions() {

    }

    @OnClick(R.id.btn_continue)
    void submit() {
        showProgressDialog();

        sendSms();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();

                showAlertDialog(R.string.error_registration_timeout);
            }
        }, SMS_REGISTRATION_TIMEOUT);
    }

    private void sendSms() {
        Uri uri = Uri.parse("smsto:" + getString(R.string.sms_shortcode));

        Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
        smsIntent.putExtra("sms_body", getString(R.string.sms_reg_body));

        startActivity(smsIntent);
    }

    private void showProgressDialog() {
        dismissProgressDialog();

        mProgress = ProgressDialog.show(this, null, getString(R.string.message_verifying));
    }

    private void dismissProgressDialog() {
        if (mProgress != null) {
            mProgress.dismiss();

            mProgress = null;
        }
    }

    public void onEventMainThread(AppData.Event event) {
        AppData.Event.Type type = event.getType();

        if (AppData.Event.Type.Verification.equals(type)) {
            mHandler.removeCallbacksAndMessages(null);
            dismissProgressDialog();

            LauncherActivity.start(this);
            finish();
        }
    }

    public void onEventMainThread(SmsBroadcastReceiver.Event event) {
        SmsBroadcastReceiver.Event.Type type = event.getType();
        String message = event.getMessage();

        if (SmsBroadcastReceiver.Event.Type.VerificationError.equals(type)) {
            mHandler.removeCallbacksAndMessages(null);
            dismissProgressDialog();

            if (!TextUtils.isEmpty(message)) {
                showAlertDialog(message);
            }
        }
    }
}
