package ph.com.globe.globeamaxretailer.ui.widget;

import android.content.Context;
import android.database.Cursor;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import ph.com.globe.globeamaxretailer.data.Contact;
import ph.com.globe.globeamaxretailer.util.MobileNumberUtil;

public class ContactsAutoCompleteTextView extends AutoCompleteTextView {
    private static final String HACKY_DELIMITER = "\u200e";

    private Listener mListener = null;

    public ContactsAutoCompleteTextView(Context context) {
        super(context);
        init();
    }

    public ContactsAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        setText(HACKY_DELIMITER);
        setSelection(getText().length());

        setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (mListener != null) {
                        String number = MobileNumberUtil.formatNumber(getText().toString());

                        if (MobileNumberUtil.isValidNumber(number)) {
                            mListener.shouldAdd(number);
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        addTextChangedListener(new TextWatcher() {
            private String prevText = null;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                prevText = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(getText().toString())) {
                    if (prevText.equals(HACKY_DELIMITER) && mListener != null) {
                        mListener.shouldDelete();
                    }

                    setText(HACKY_DELIMITER);
                    setSelection(getText().length());

                } else if (getText().toString().startsWith(HACKY_DELIMITER) && getText().length() > 1) {
                    setText(getText().toString().substring(1));
                    setSelection(getText().length());
                }
            }
        });

        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (mListener != null) {
                    mListener.onFocusChanged(hasFocus);
                }
                if (!hasFocus) {
                    if (mListener != null) {
                        String number = MobileNumberUtil.formatNumber(getText().toString());

                        if (MobileNumberUtil.isValidNumber(number)) {
                            mListener.shouldAdd(number);
                        }
                        else {
                            mListener.onError();
                        }
                    }
                }
            }
        });
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        Cursor cursor = (Cursor) selectedItem;

        Contact contact = new Contact();
        contact.loadFromCursor(cursor);

        return contact.name + " <" + contact.number + ">";
    }

    @Override
    public void onFilterComplete(int count) {
        try {
            super.onFilterComplete(count);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface Listener {
        void shouldDelete();
        void shouldAdd(String number);
        void onError();
        void onFocusChanged(boolean hasFocus);
    }
}
