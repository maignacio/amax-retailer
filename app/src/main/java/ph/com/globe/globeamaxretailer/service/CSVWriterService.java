package ph.com.globe.globeamaxretailer.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;

import java.io.File;

import au.com.bytecode.opencsv.CSV;
import au.com.bytecode.opencsv.CSVWriteProc;
import au.com.bytecode.opencsv.CSVWriter;
import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.data.TransactionLog;
import ph.com.globe.globeamaxretailer.util.DateTimeFormatHelper;

public class CSVWriterService extends IntentService {

    private static final CSV csv = CSV
            .separator(',')
            .quote('"')
            .charset("UTF-8")
            .create();

    public CSVWriterService() {
        super("CSVWriterService");
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, CSVWriterService.class);

        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String storageState = Environment.getExternalStorageState();

            if (!storageState.equals(Environment.MEDIA_MOUNTED)) {
                EventBus.getDefault().post(new Event(Event.Type.Fail));
                return;
            }

            try {
                String directory = new Uri.Builder().path(Environment.getExternalStorageDirectory().getAbsolutePath()).appendPath("amax").toString();

                // create a File object for the parent directory
                File csvDirectory = new File(directory);
                // have the object build the directory structure, if needed.
                csvDirectory.mkdirs();

                String fileName = new Uri.Builder().path(directory).appendPath(System.currentTimeMillis() + ".csv").toString();

                // CSVWriter will be closed after end of processing
                csv.write(fileName, new CSVWriteProc() {
                    public void process(CSVWriter out) {
                        out.writeNext("Date", "Time", "Denomination", "Retailer Price", "Recipient", "Ref No.");

                        Cursor cursor = TransactionLog.getAllTransactionLogs();

                        TransactionLog log = null;
                        long timeInMillis = -1;

                        while (cursor.moveToNext()) {
                            log = new TransactionLog();
                            log.loadFromCursor(cursor);

                            timeInMillis = log.datetime.getTimeInMillis();

                            out.writeNext(DateTimeFormatHelper.formatDate(timeInMillis), DateTimeFormatHelper.formatTime(timeInMillis), "P" + log.denomination, "P" + log.retailerValue, log.recipient, log.refNumber);
                        }
                    }
                });

                Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri fileContentUri = Uri.fromFile(new File(fileName)); // With 'permFile' being the File object
                mediaScannerIntent.setData(fileContentUri);
                sendBroadcast(mediaScannerIntent);

                //MediaScannerConnection.scanFile(this, new String[]{csvDirectory.getAbsolutePath()}, null, null);

                Event event = new Event(Event.Type.Success);
                event.fileName = fileName;

                EventBus.getDefault().post(event);
            } catch (Exception e) {
                e.printStackTrace();
                EventBus.getDefault().post(new Event(Event.Type.Fail));
            }
        }
    }

    public static class Event {
        public Type type = Type.Success;
        public String fileName = null;
        public Event(Type type) {
            this.type = type;
        }

        public enum Type {
            Success, Fail
        }
    }
}
