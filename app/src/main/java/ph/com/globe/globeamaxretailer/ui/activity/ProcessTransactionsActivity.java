package ph.com.globe.globeamaxretailer.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.List;

import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.Transaction;
import ph.com.globe.globeamaxretailer.util.AlarmManagerHelper;
import ph.com.globe.globeamaxretailer.util.StaticWakeLock;
import ph.com.globe.globeamaxretailer.util.UssdHelper;

public class ProcessTransactionsActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        View v = getLayoutInflater().inflate(R.layout.activity_process_transactions, null);
        v.setKeepScreenOn(true);
        setContentView(v);

        getSupportActionBar().hide();

        process();
    }

    private void process() {
        List<Transaction> transactions = Transaction.getTransactionsToProcess(System.currentTimeMillis());
        int count = transactions.size();

        int delay = 0;

        Handler handler = new Handler(Looper.getMainLooper());

        for (int i = 0; i < count; i++) {
            delay = i * 15000;

            final Transaction transaction = transactions.get(i);
            final boolean last = (i == (count - 1));

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (last) {
                        isLast = true;
                    }

                    Transaction.createNextTransactionFromFinishedTransaction(transaction);

                    UssdHelper.sendLoadViaDirectUssd(ProcessTransactionsActivity.this, transaction.recipient, String.valueOf(transaction.schedule.denomination));
                }
            }, delay);
        }
    }

    private boolean isLast = false;

    @Override
    protected void onResume() {
        super.onResume();

        if (isLast) {
            AlarmManagerHelper.setAlarm(getApplicationContext());

            EventBus.getDefault().post(new Event());

            finish();
        }
    }

    @Override
    protected void onDestroy() {
        StaticWakeLock.lockOff(this);
        super.onDestroy();
    }

    public static class Event {
    }
}
