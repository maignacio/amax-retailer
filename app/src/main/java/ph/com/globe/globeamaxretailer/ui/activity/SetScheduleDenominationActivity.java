package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import java.io.Serializable;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import ph.com.globe.globeamaxretailer.R;

public class SetScheduleDenominationActivity extends BaseSchedulerActivity {
    @Bind(R.id.input_amount)
    EditText mInputAmount;

    private Button mSelectedButton;

    private String mTitle = null;
    private List<String> mRecipients = null;

    public static void start(Activity activity, long scheduleId, String title, List<String> recipients) {
        Intent intent = new Intent(activity, SetScheduleDenominationActivity.class);
        intent.putExtra(Intent.EXTRA_REFERRER, scheduleId);
        intent.putExtra(Intent.EXTRA_TITLE, title);
        intent.putExtra(Intent.EXTRA_PHONE_NUMBER, (Serializable) recipients);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_schedule_denomination);

        enableBackButton();

        if (mExtras == null) {
            finish();
            return;
        }

        mTitle = mExtras.getString(Intent.EXTRA_TITLE);
        mRecipients = (List<String>) mExtras.get(Intent.EXTRA_PHONE_NUMBER);

        mInputAmount.addTextChangedListener(new TextWatcher() {
            private boolean wasEmpty = true;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                wasEmpty = !(s.length() > 0);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString())) {
                    selectDenominationButton(null);
                } else if (TextUtils.isEmpty(s.toString()) && !wasEmpty) {
                    wasEmpty = true;
                    selectDenominationButton(null);
                }
            }
        });
    }

    @Override
    void updateInBackground() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mInputAmount.setText(mSchedule.denomination + "");
            }
        });
    }

    @OnClick({R.id.btn_denom_1, R.id.btn_denom_2, R.id.btn_denom_3, R.id.btn_denom_4, R.id.btn_denom_5, R.id.btn_denom_6, R.id.btn_denom_7, R.id.btn_denom_8, R.id.btn_denom_9})
    void selectDenominationButton(Button button) {
        if (mSelectedButton != null) {
            mSelectedButton.setSelected(false);
            mSelectedButton = null;
        }

        if (button != null) {
            mInputAmount.setText(null);

            mSelectedButton = button;
            mSelectedButton.setSelected(true);
        }
    }

    @OnClick(R.id.btn_submit)
    void submit() {
        String denomination = mInputAmount.getText().toString();

        if (mSelectedButton != null) {
            denomination = mSelectedButton.getText().toString().substring(1);
        }

        if (TextUtils.isEmpty(denomination) || !isDenominationValid(denomination)) {
            showAlertDialog(R.string.error_set_schedule_denomination_invalid);
            return;
        }

        SetScheduleFrequencyActivity.start(SetScheduleDenominationActivity.this, mScheduleId, mTitle, mRecipients, denomination);
    }

    private boolean isDenominationValid(String amount) {
        try {
            int denomination = Integer.valueOf(amount);

            if (denomination >= 10 && denomination <= 150) {
                return true;
            }

            switch (denomination) {
                case 350:
                case 450:
                case 550:
                case 600:
                case 700:
                case 900:
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
