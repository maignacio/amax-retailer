package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.Contact;
import ph.com.globe.globeamaxretailer.data.Schedule;
import ph.com.globe.globeamaxretailer.util.AlarmManagerHelper;
import ph.com.globe.globeamaxretailer.util.DateTimeFormatHelper;

public class SetScheduleFrequencyActivity extends BaseSchedulerActivity {
    @Bind(R.id.btn_frequency)
    Button mBtnFrequencyPicker;
    @Bind(R.id.btn_start_date)
    Button mBtnStartDatePicker;
    @Bind(R.id.btn_end_date)
    Button mBtnEndDatePicker;
    @Bind(R.id.btn_time)
    Button mBtnTimePicker;

    private String mTitle = null;
    private List<String> mRecipients = null;
    private String mDenomination = null;

    private int mScheduleFrequency = 0;
    private Calendar mStartDate = null;
    private Calendar mEndDate = null;
    private int mChosenHour = -1;
    private int mChosenMinute = -1;

    private String[] mFrequencyValues = null;

    public static void start(Activity activity, long scheduleId, String title, List<String> recipients, String denomination) {
        Intent intent = new Intent(activity, SetScheduleFrequencyActivity.class);
        intent.putExtra(Intent.EXTRA_REFERRER, scheduleId);
        intent.putExtra(Intent.EXTRA_TITLE, title);
        intent.putExtra(Intent.EXTRA_PHONE_NUMBER, (Serializable) recipients);
        intent.putExtra(Intent.EXTRA_TEXT, denomination);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_schedule_frequency);

        enableBackButton();

        if (mExtras == null) {
            finish();
            return;
        }

        mFrequencyValues = getResources().getStringArray(R.array.values_frequency);

        mTitle = mExtras.getString(Intent.EXTRA_TITLE);
        mRecipients = (List<String>) mExtras.get(Intent.EXTRA_PHONE_NUMBER);
        mDenomination = mExtras.getString(Intent.EXTRA_TEXT);

        registerForContextMenu(mBtnFrequencyPicker);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Choose Frequency");

        int count = mFrequencyValues.length;

        for (int i = 0; i < count; i++) {
            menu.add(Menu.NONE, i, Menu.NONE, mFrequencyValues[i]);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        updateFrequency(item.getItemId());

        return true;
    }

    @Override
    void updateInBackground() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                updateFrequency(mSchedule.frequency);
                updateStartTime(mSchedule.start);
                updateEndTime(mSchedule.end);
                updateTime(mSchedule.start.get(Calendar.HOUR_OF_DAY), mSchedule.start.get(Calendar.MINUTE));
            }
        });
    }

    @OnClick(R.id.btn_frequency)
    void pickFrequency(Button button) {
        openContextMenu(button);
    }

    @OnClick(R.id.btn_start_date)
    void pickStartDate(final Button button) {
        DatePickerDialog.Builder builder = new DatePickerDialog.Builder() {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                DatePickerDialog dialog = (DatePickerDialog) fragment.getDialog();

                String formattedDate = dialog.getFormattedDate(SimpleDateFormat.getDateInstance());

                Log.d(SetScheduleFrequencyActivity.class.getSimpleName(), formattedDate);

                updateStartTime(dialog.getCalendar());

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        if (mEndDate != null) {
            builder.dateRange(0, mEndDate.getTimeInMillis());
        }
        builder.date((mStartDate != null) ? mStartDate.getTimeInMillis() : System.currentTimeMillis());
        builder.positiveAction(getString(android.R.string.ok))
                .negativeAction(getString(android.R.string.cancel));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
    }

    @OnClick(R.id.btn_end_date)
    void pickEndDate(final Button button) {
        DatePickerDialog.Builder builder = new DatePickerDialog.Builder() {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                DatePickerDialog dialog = (DatePickerDialog) fragment.getDialog();
                String formattedDate = dialog.getFormattedDate(SimpleDateFormat.getDateInstance());

                Log.d(SetScheduleFrequencyActivity.class.getSimpleName(), formattedDate);

                updateEndTime(dialog.getCalendar());

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        if (mStartDate != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(mStartDate.getTimeInMillis());
            calendar.add(Calendar.YEAR, 100);

            builder.dateRange(mStartDate.getTimeInMillis(), calendar.getTimeInMillis());
        }
        builder.date((mEndDate != null) ? mEndDate.getTimeInMillis() : System.currentTimeMillis());
        builder.positiveAction(getString(android.R.string.ok))
                .negativeAction(getString(android.R.string.cancel));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
    }

    @OnClick(R.id.btn_time)
    void pickTime(final Button button) {
        TimePickerDialog.Builder builder = new TimePickerDialog.Builder() {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                TimePickerDialog dialog = (TimePickerDialog) fragment.getDialog();
                updateTime(dialog.getHour(), dialog.getMinute());

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        if (mChosenHour < 0 && mChosenMinute < 0) {
            builder.hour(8);
            builder.minute(0);

        } else {
            builder.hour(mChosenHour);
            builder.minute(mChosenMinute);

        }

        builder.positiveAction(getString(android.R.string.ok))
                .negativeAction(getString(android.R.string.cancel));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
    }

    @OnClick(R.id.btn_submit)
    void submit() {
        if (mStartDate == null || mEndDate == null) {
            showAlertDialog(R.string.error_set_schedule_frequency_select_date);
            return;
        }

        if (mChosenHour < 0 || mChosenMinute < 0) {
            showAlertDialog(R.string.error_set_schedule_frequency_select_time);
            return;
        }

        mStartDate.set(Calendar.AM_PM, Calendar.AM);
        mStartDate.set(Calendar.HOUR_OF_DAY, mChosenHour);
        mStartDate.set(Calendar.MINUTE, mChosenMinute);
        mStartDate.set(Calendar.SECOND, 0);
        mStartDate.set(Calendar.MILLISECOND, 0);

        mEndDate.set(Calendar.AM_PM, Calendar.AM);
        mEndDate.set(Calendar.HOUR_OF_DAY, mChosenHour);
        mEndDate.set(Calendar.MINUTE, mChosenMinute);
        mEndDate.set(Calendar.SECOND, 0);
        mEndDate.set(Calendar.MILLISECOND, 0);

        showConfirmationMessage();
    }

    private void updateFrequency(int frequency) {
        mScheduleFrequency = frequency;
        String text = mFrequencyValues[frequency];

        mBtnFrequencyPicker.setText(text);
    }

    private void updateStartTime(Calendar calendar) {
        mStartDate = calendar;
        mBtnStartDatePicker.setText(DateTimeFormatHelper.formatDate(calendar.getTimeInMillis()));
    }

    private void updateEndTime(Calendar calendar) {
        mEndDate = calendar;
        mBtnEndDatePicker.setText(DateTimeFormatHelper.formatDate(calendar.getTimeInMillis()));
    }

    private void updateTime(int hour, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.AM_PM, Calendar.AM);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        mChosenHour = hour;
        mChosenMinute = minute;

        mBtnTimePicker.setText(DateTimeFormatHelper.formatTime(cal.getTimeInMillis()));
    }

    private void showConfirmationMessage() {
        mThreadPool.submit(new Runnable() {
            @Override
            public void run() {
                StringBuilder builder = new StringBuilder();
                int count = mRecipients.size();
                Contact contact = null;

                for (int i = 0; i < count; i++) {
                    contact = Contact.getContactFromPhone(mRecipients.get(i));

                    if (i > 0) {
                        builder.append("\n");
                    }

                    if (contact != null) {
                        builder.append(contact.name);
                        builder.append(" - ");
                        builder.append(contact.number);
                    } else {
                        builder.append(mRecipients.get(i));
                    }
                }

                final String recipients = builder.toString();

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SetScheduleFrequencyActivity.this);
                        builder.setTitle(R.string.alert_schedule_confirmation_title);
                        View view = View.inflate(SetScheduleFrequencyActivity.this, R.layout.alert_create_schedule_confirmation, null);

                        String frequency = mFrequencyValues[mScheduleFrequency];

                        long startTimestamp = mStartDate.getTimeInMillis();
                        long endTimestamp = mEndDate.getTimeInMillis();

                        ((TextView) view.findViewById(R.id.title)).setText(mTitle);
                        ((TextView) view.findViewById(R.id.recipients)).setText(recipients);
                        ((TextView) view.findViewById(R.id.frequency)).setText(frequency);
                        ((TextView) view.findViewById(R.id.amount)).setText(getString(R.string.peso) + mDenomination);
                        ((TextView) view.findViewById(R.id.date)).setText(DateTimeFormatHelper.formatDate(startTimestamp) + " - " + DateTimeFormatHelper.formatDate(endTimestamp));
                        ((TextView) view.findViewById(R.id.time)).setText(DateTimeFormatHelper.formatTime(startTimestamp));

                        builder.setView(view);
                        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                createSchedule();
                            }
                        });
                        builder.setNegativeButton(android.R.string.cancel, null);

                        AppCompatDialog dialog = builder.create();
                        dialog.show();
                    }
                });
            }
        });
    }

    private void createSchedule() {
        mThreadPool.submit(new Runnable() {
            @Override
            public void run() {
                if (mScheduleId > -1) {
                    Schedule.updateSchedule(mScheduleId, mTitle, mStartDate, mEndDate, Integer.parseInt(mDenomination), mScheduleFrequency, mRecipients);
                } else {
                    Schedule.createSchedule(mTitle, mStartDate, mEndDate, Integer.parseInt(mDenomination), mScheduleFrequency, mRecipients);
                }

                AlarmManagerHelper.setAlarm(getApplicationContext());
            }
        });
    }
}
