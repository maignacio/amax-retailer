package ph.com.globe.globeamaxretailer.data;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Promo", id = "_id")
public class Promo extends Model {
    public static final int TYPE_ALL = 0;
    public static final int TYPE_GLOBE = 1;
    public static final int TYPE_TM = 2;
    public static final int TYPE_TATTOO = 3;

    @Column(name = "title")
    public String title;

    @Column(name = "ussd_string")
    public String ussdString;

    @Column(name = "parent_ussd_string")
    public String parentUssdString;

    @Column(name = "icon")
    public String icon;

    @Column(name = "type")
    public int type;

    public static List<Promo> getList(String parentUssdString) {
        return getSQLFromToRetrieveChildPromos(parentUssdString).execute();
    }

    public static int getType(String ussdString) {
        Promo promo = new Select().from(Promo.class).where("ussd_string = ?", new String[]{ussdString}).executeSingle();

        if (promo != null)
            return promo.type;

        return 0;
    }

    @NonNull
    private static From getSQLFromToRetrieveChildPromos(String parentUssdString) {
        From from = new Select().from(Promo.class);

        if (TextUtils.isEmpty(parentUssdString)) {
            from = from.where("parent_ussd_string IS NULL");
        } else {
            from = from.where("parent_ussd_string = ?", new String[]{parentUssdString});
        }
        return from;
    }

    public boolean doesPromoHaveChildren() {
        int count = getSQLFromToRetrieveChildPromos(this.ussdString).count();

        return count > 0;
    }
}
