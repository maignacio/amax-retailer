package ph.com.globe.globeamaxretailer.data;

import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Contacts", id = "_id")
public class Contact extends Model {
    @Column(name = "name", notNull = true)
    public String name;

    @Column(name = "number", notNull = true)
    public String number;

    @Column(name = "uri", notNull = true)
    public Uri uri;

    public static Contact getContactFromPhone(String number) {
        return new Select().from(Contact.class).where("number = '" + number + "'").orderBy("name ASC").executeSingle();
    }

    public static Cursor getCursor(String query, String extraSqlClause) {
        String sql = new Select().from(Contact.class).where("(number LIKE '" + query + "%' OR name LIKE '" + query + "%' OR name LIKE '% " + query + "%'" + ((TextUtils.isEmpty(extraSqlClause)) ? ")" : ") AND " + extraSqlClause)).orderBy("name ASC").toSql();
        return ActiveAndroid.getDatabase().rawQuery(sql, null);
    }

    @Override
    public String toString() {
        return "Contact object {name: " + name + ", number: " + number + ", uri: " + uri.toString() + "}";
    }
}
