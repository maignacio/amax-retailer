package ph.com.globe.globeamaxretailer.data;

import android.database.Cursor;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Calendar;
import java.util.List;

import de.greenrobot.event.EventBus;

@Table(name = "Schedules", id = "_id")
public class Schedule extends Model {
    @Column(name = "name")
    public String name;

    @Column(name = "start", notNull = true)
    public Calendar start;

    @Column(name = "end", notNull = true)
    public Calendar end;

    @Column(name = "denomination", notNull = true)
    public int denomination;

    @Column(name = "frequency", notNull = true)
    public int frequency;

    public static Cursor getAllSchedules() {
        String sql = new Select().from(Schedule.class).orderBy("end DESC").toSql();
        return ActiveAndroid.getDatabase().rawQuery(sql, null);
    }

    public static List<Recipient> getRecipients(int id) {
        return new Select().from(Recipient.class).where("schedule = " + id).orderBy("number ASC").execute();
    }

    public static Schedule createSchedule(String name, Calendar start, Calendar end, int denomination, int frequency, List<String> recipients) {
        return setSchedule(null, name, start, end, denomination, frequency, recipients);
    }

    public static Schedule updateSchedule(long id, String name, Calendar start, Calendar end, int denomination, int frequency, List<String> recipients) {
        // Delete previous data
        Transaction.deletePendingTransactionsFromSchedule(id);
        Recipient.deleteRecipientsOfSchedule(id);

        return setSchedule(Schedule.load(Schedule.class, id), name, start, end, denomination, frequency, recipients);
    }

    public static void deleteSchedule(long id) {
        Transaction.deleteTransactionsFromSchedule(id);
        Recipient.deleteRecipientsOfSchedule(id);

        Schedule.delete(Schedule.class, id);

        EventBus.getDefault().post(new Event());
    }

    private static Schedule setSchedule(Schedule schedule, String name, Calendar start, Calendar end, int denomination, int frequency, List<String> recipients) {
        boolean isNewSchedule = false;
        long currentTime = System.currentTimeMillis();

        if (schedule == null) {
            isNewSchedule = true;
            schedule = new Schedule();
        }

        ActiveAndroid.beginTransaction();
        try {
            // Update schedule data
            schedule.name = name;
            schedule.start = start;
            schedule.end = end;
            schedule.denomination = denomination;
            schedule.frequency = frequency;
            schedule.save();

            // Create recipient and transation data
            Recipient recipient = null;
            Transaction transaction = null;

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(schedule.start.getTimeInMillis());

            if (frequency != Frequency.ONCE_ONLY) {
                if (frequency == Frequency.EVERY_29TH) {
                    calendar.set(Calendar.DATE, 29);

                    if (calendar.getTimeInMillis() < start.getTimeInMillis()) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                }

                while (calendar.getTimeInMillis() < currentTime) {
                    calendar = Transaction.createNextTransactionDatetime(schedule, calendar);
                }
            }

            if (schedule.frequency == Frequency.EVERY_29TH && isNewSchedule) {
                calendar.set(Calendar.DATE, 29);

                if (calendar.compareTo(schedule.start) < 0) {
                    calendar.add(Calendar.MONTH, 1);
                }
            }

            for (String number : recipients) {
                recipient = new Recipient();
                recipient.number = number;
                recipient.schedule = schedule;
                recipient.save();

                transaction = new Transaction();
                transaction.schedule = schedule;
                transaction.recipient = number;
                transaction.datetime = calendar;
                transaction.denomination = denomination;
                transaction.status = Transaction.Status.Pending;
                transaction.save();
            }

            ActiveAndroid.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            ActiveAndroid.endTransaction();
            EventBus.getDefault().post(new Event());
        }

        return schedule;
    }

    public interface Frequency {
        int ONCE_ONLY = 0;
        int DAILY = 1;
        int EVERY_7_DAYS = 2;
        int EVERY_15_DAYS = 3;
        int EVERY_29TH = 4;
    }

    public static class Event {
    }
}
