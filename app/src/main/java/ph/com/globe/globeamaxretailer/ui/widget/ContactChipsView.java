package ph.com.globe.globeamaxretailer.ui.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.squareup.picasso.Picasso;

import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.Contact;
import ph.com.globe.globeamaxretailer.ui.widget.imageview.transform.CircleTransform;

public class ContactChipsView extends LinearLayout {
    private ImageView avatar;
    private TextView displayName;
    private ColorGenerator colorGenerator;

    private Contact contact = null;

    public ContactChipsView(Context context) {
        super(context);
        init();
    }

    public ContactChipsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.widget_contact_chip, this);
        setOrientation(HORIZONTAL);
        setBackgroundResource(R.drawable.chip);

        colorGenerator = ColorGenerator.DEFAULT;

        avatar = (ImageView) findViewById(R.id.avatar);
        displayName = (TextView) findViewById(R.id.displayName);
    }

    public void setContact(Contact contact) {
        this.contact = contact;

        if (TextUtils.isEmpty(this.contact.name)) {
            this.contact.name = this.contact.number;
        }

        TextDrawable drawable = TextDrawable.builder().buildRound(this.contact.name.toUpperCase().substring(0, 1), colorGenerator.getColor(this.contact.name));

        if (contact.uri != null) {
            Picasso.with(getContext()).load(this.contact.uri).error(drawable).placeholder(drawable).transform(new CircleTransform()).into(avatar);
        } else {
            avatar.setImageDrawable(drawable);
        }

        displayName.setText(this.contact.name);
    }
}
