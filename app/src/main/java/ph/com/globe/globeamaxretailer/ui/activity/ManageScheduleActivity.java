package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.Bind;
import butterknife.OnItemClick;
import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.cache.RecipientsCache;
import ph.com.globe.globeamaxretailer.data.Recipient;
import ph.com.globe.globeamaxretailer.data.Schedule;
import ph.com.globe.globeamaxretailer.util.DateTimeFormatHelper;
import ph.com.globe.globeamaxretailer.util.ViewHolder;

public class ManageScheduleActivity extends BaseActivity {
    @Bind(android.R.id.list)
    ListView mListView;

    private ExecutorService mThreadPool = Executors.newSingleThreadExecutor();
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private ListAdapter mAdapter;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ManageScheduleActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus.getDefault().register(this);

        setContentView(R.layout.activity_list);

        enableBackButton();

        mAdapter = new ListAdapter(this);

        View empty = getLayoutInflater().inflate(R.layout.view_manage_schedules_empty, null);
        addContentView(empty, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mListView.setEmptyView(empty);
        mListView.setAdapter(mAdapter);

        update();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }

    @OnItemClick(android.R.id.list)
    void select(View view, int position) {
        long id = (long) view.getTag(R.id.tag_schedule);

        if (id > -1) {
            SetScheduleRecipientActivity.start(ManageScheduleActivity.this, id);
        }
    }

    private void update() {
        mThreadPool.submit(new Runnable() {
            @Override
            public void run() {
                final Cursor cursor = Schedule.getAllSchedules();

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.swapCursor(cursor);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    public void onEventMainThread(RecipientsCache.Event event) {
        mAdapter.notifyDataSetChanged();
    }

    public void onEventMainThread(Schedule.Event event) {
        update();
    }

    private class ListAdapter extends CursorAdapter {
        private LayoutInflater mInflater;
        private String[] mFrequencyValues;

        private View.OnClickListener mDeleteButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final long id = (long) ((View) v.getParent()).getTag(R.id.tag_schedule);
                if (id > -1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ManageScheduleActivity.this);
                    builder.setTitle(R.string.alert_manage_schedule_title_delete)
                            .setMessage(R.string.alert_manage_schedule_body_delete)
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Schedule.deleteSchedule(id);
                                }
                            });

                    AppCompatDialog dialog = builder.create();
                    dialog.show();
                }
            }
        };

        public ListAdapter(Context context) {
            super(context, null, true);

            mInflater = LayoutInflater.from(context);
            mFrequencyValues = context.getResources().getStringArray(R.array.values_frequency);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return mInflater.inflate(R.layout.item_manage_schedule, null);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            Schedule schedule = new Schedule();
            schedule.loadFromCursor(cursor);

            String recipientsString = null;

            List<Recipient> recipients = RecipientsCache.getEntry(schedule.getId().intValue());
            if (recipients != null) {
                int count = recipients.size();

                StringBuilder builder = new StringBuilder();

                for (int i = 0; i < count; i++) {
                    if (i > 0) {
                        builder.append(", ");
                    }

                    builder.append(recipients.get(i).name);
                }

                recipientsString = builder.toString();
            } else {
                RecipientsCache.enqueueContactPhotoRequest(context, schedule.getId().intValue());
            }

            TextView title = ViewHolder.get(view, R.id.title);
            TextView numbers = ViewHolder.get(view, R.id.recipients);
            TextView value = ViewHolder.get(view, R.id.value);
            TextView time = ViewHolder.get(view, R.id.time);
            ImageButton delete = ViewHolder.get(view, R.id.btn_delete);

            title.setText((TextUtils.isEmpty(schedule.name)) ? recipientsString : schedule.name);
            numbers.setText(recipientsString);
            value.setText(context.getString(R.string.peso) + schedule.denomination + " " + mFrequencyValues[schedule.frequency]);
            time.setText(DateTimeFormatHelper.formatDate(schedule.start.getTimeInMillis()) + " to " + DateTimeFormatHelper.formatDate(schedule.end.getTimeInMillis()) + " at " + DateTimeFormatHelper.formatTime(schedule.start.getTimeInMillis()));

            delete.setOnClickListener(mDeleteButtonOnClickListener);

            view.setTag(R.id.tag_schedule, schedule.getId());
        }
    }
}
