package ph.com.globe.globeamaxretailer.data;

import android.content.Context;

import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.util.PreferencesHelper;

public class AppData {
    private static final String IS_VERIFIED = "a";
    private static final String SIM_SERIAL = "b";

    public static void setUserAsVerified(Context context) {
        PreferencesHelper.getInstance(context).putBoolean(IS_VERIFIED, true);

        EventBus.getDefault().post(new Event(Event.Type.Verification));
    }

    public static boolean isUserVerified(Context context) {
        return PreferencesHelper.getInstance(context).getBoolean(IS_VERIFIED, false);
    }

    public static void setSimSerialNumber(Context context, String serialNumber) {
        PreferencesHelper.getInstance(context).putString(SIM_SERIAL, serialNumber);
    }

    public static String getSimSerialNumber(Context context) {
        return PreferencesHelper.getInstance(context).getString(SIM_SERIAL, null);
    }

    public static void reset(Context context) {
        PreferencesHelper mPreferencesHelper = PreferencesHelper.getInstance(context);
        mPreferencesHelper.putString(SIM_SERIAL, null);
        mPreferencesHelper.putBoolean(IS_VERIFIED, false);
    }

    public static class Event {
        public enum Type {
            Verification
        }

        private Type mType = Type.Verification;

        public Event(Type type) {
            mType = type;
        }

        public Type getType() {
            return mType;
        }
    }
}

