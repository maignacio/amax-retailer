package ph.com.globe.globeamaxretailer.cache;

import android.content.Context;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.data.Contact;

public class ContactsCache {
    private static HashMap<String, String> mCache = null;
    private static LinkedList<String> mContactIdFetchRequests = null;
    private static ExecutorService pool = null;

    private ContactsCache() {

    }

    private static HashMap<String, String> getCache() {
        if (mCache == null) {
            mCache = new HashMap<>();
        }

        if (mContactIdFetchRequests == null) {
            mContactIdFetchRequests = new LinkedList<>();
        }

        if (pool == null) {
            pool = Executors.newSingleThreadExecutor();
        }

        return mCache;
    }

    public static String getEntry(String key) {
        return getCache().get(key);
    }

    private static void setEntry(String key, String value) {
        getCache().put(key, value);
    }

    public static void clearCache() {
        getCache().clear();
        EventBus.getDefault().post(new Event());
    }

    public static void enqueueContactPhotoRequest(final Context context, String number) {
        getCache();
        synchronized (mContactIdFetchRequests) {
            mContactIdFetchRequests.add(number);
        }
        pool.submit(new Runnable() {

            @Override
            public void run() {
                synchronized (mContactIdFetchRequests) {
                    String number = null;
                    if (!mContactIdFetchRequests.isEmpty()) {
                        number = mContactIdFetchRequests.removeFirst();

                        getContactIDFromNumber(context, number);
                    }
                }
            }
        });
    }

    private static void getContactIDFromNumber(Context context, String number) {
        String key = number;
        String value = number;
        try {
            Contact contact = Contact.getContactFromPhone(number);

            if (contact != null && !TextUtils.isEmpty(contact.name)) {
                value = contact.name;
            }

            setEntry(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mContactIdFetchRequests.isEmpty()) {
            EventBus.getDefault().post(new Event());
        }
    }

    public static class Event {

    }
}
