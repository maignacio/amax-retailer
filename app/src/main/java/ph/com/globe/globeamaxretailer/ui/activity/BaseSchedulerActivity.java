package ph.com.globe.globeamaxretailer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.data.Schedule;

public abstract class BaseSchedulerActivity extends BaseActivity {
    protected ExecutorService mThreadPool = Executors.newSingleThreadExecutor();
    protected Handler mHandler = new Handler(Looper.getMainLooper());

    protected long mScheduleId = -1;
    protected Schedule mSchedule = null;
    protected Bundle mExtras = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus.getDefault().register(this);

        mExtras = getIntent().getExtras();

        if (mExtras != null) {
            mScheduleId = mExtras.getLong(Intent.EXTRA_REFERRER, -1);

            if (mScheduleId > -1) {
                mThreadPool.submit(new Runnable() {
                    @Override
                    public void run() {
                        mSchedule = Schedule.load(Schedule.class, mScheduleId);
                        update();
                    }
                });
            }
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }

    protected void update() {
        mThreadPool.submit(new Runnable() {
            @Override
            public void run() {
                if (mSchedule != null) {
                    updateInBackground();
                }
            }
        });
    }

    abstract void updateInBackground();

    public void onEventMainThread(Schedule.Event event) {
        finish();
    }
}
