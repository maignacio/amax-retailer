package ph.com.globe.globeamaxretailer.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Map;

public class PreferencesHelper {
    private static PreferencesHelper INSTANCE = null;
    private Context mContext;
    private SharedPreferences mPrefs;

    private PreferencesHelper(Context context) {
        mContext = context.getApplicationContext();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    /**
     * Returns a re-usable PreferenceUtil instance
     *
     * @param context context where this object will be used
     * @return re-usable PreferenceUtil instance object
     */

    public static synchronized PreferencesHelper getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new PreferencesHelper(context.getApplicationContext());
        }

        return INSTANCE;
    }

    public Map<String, ?> getAllValues() {
        return mPrefs.getAll();
    }

    /**
     * Retrieve a String value from preferences
     *
     * @param key      name of the preference to retrieve
     * @param defValue value to return if the preference does not exist
     * @return preference value or defValue if the preference does not exist
     */

    public synchronized String getString(String key, String defValue) {
        return mPrefs.getString(key, defValue);
    }

    /**
     * Set a String value to preferences
     *
     * @param key   name of the preference to modify
     * @param value new value of the preference. setting the value to null deletes
     *              the preference from storage
     */

    public synchronized void putString(String key, String value) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Retrieve a boolean value from preferences
     *
     * @param key      name of the preference to retrieve
     * @param defValue value to return if the preference does not exist
     * @return preference value or defValue if the preference does not exist
     */

    public boolean getBoolean(String key, boolean defValue) {
        return mPrefs.getBoolean(key, defValue);
    }

    /**
     * Set a boolean value to preferences
     *
     * @param key   name of the preference to modify
     * @param value new value of the preference
     */

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Retrieve a int value from preferences
     *
     * @param key      name of the preference to retrieve
     * @param defValue value to return if the preference does not exist
     * @return preference value or defValue if the preference does not exist
     */

    public int getInt(String key, int defValue) {
        return mPrefs.getInt(key, defValue);
    }

    /**
     * Set a int value to preferences
     *
     * @param key   name of the preference to modify
     * @param value new value of the preference
     */

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * Retrieve a long value from preferences
     *
     * @param key      name of the preference to retrieve
     * @param defValue value to return if the preference does not exist
     * @return preference value or defValue if the preference does not exist
     */

    public long getLong(String key, long defValue) {
        return mPrefs.getLong(key, defValue);
    }

    /**
     * Set a long value to preferences
     *
     * @param key   name of the preference to modify
     * @param value new value of the preference
     */

    public void putLong(String key, long value) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * Clear all preference values
     */

    public void clear() {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.clear();
        editor.commit();
    }
}
