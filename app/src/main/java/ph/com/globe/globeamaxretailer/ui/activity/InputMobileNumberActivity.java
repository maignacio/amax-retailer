package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.text.Html;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.OnClick;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.Promo;
import ph.com.globe.globeamaxretailer.util.MobileNumberUtil;
import ph.com.globe.globeamaxretailer.util.UssdHelper;

public class InputMobileNumberActivity extends BaseActivity {
    @Bind(R.id.text_header)
    TextView mHeaderText;
    @Bind(R.id.input_mobile_number)
    EditText mInputMobileNumber;

    private String mTitle = null;
    private String mUssdString = null;

    public static void start(Activity activity, String ussdString, String title) {
        Intent intent = new Intent(activity, InputMobileNumberActivity.class);
        intent.putExtra(Intent.EXTRA_TITLE, title);
        intent.putExtra(Intent.EXTRA_TEXT, ussdString);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();

        if (extras == null) {
            finish();
            return;
        }

        mUssdString = extras.getString(Intent.EXTRA_TEXT);
        mTitle = extras.getString(Intent.EXTRA_TITLE);

        setContentView(R.layout.activity_input_mobile_number);

        enableBackButton();

        setTitle(mTitle);

        String headerText = getString(R.string.header_activity_input_mobile_number).replaceAll(getString(R.string.header_activity_input_mobile_number_key_promo), mTitle);

        mHeaderText.setText(Html.fromHtml(headerText));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri contactUri = data.getData();
            if (null == contactUri) return;

            Cursor cursor = getContentResolver().query(contactUri, new String[]{ContactsContract.CommonDataKinds.Phone.DATA}, null, null, null);
            if (null == cursor) return;
            try {
                cursor.moveToFirst();
                String number = cursor.getString(0);

                mInputMobileNumber.setText(MobileNumberUtil.formatNumber(number));
            } finally {
                cursor.close();
            }
        }
    }

    @OnClick(R.id.btn_contacts)
    void pickContact() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(intent, 100);
        } catch (Exception e) {
            e.printStackTrace();
            showAlertDialog(R.string.error_send_load_no_phonebook);
        }
    }

    @OnClick(R.id.btn_send_promo)
    void submit() {
        final String mobileNumber = MobileNumberUtil.formatNumber(mInputMobileNumber.getText().toString());

        int type = Promo.getType(mUssdString);

        boolean isValid = false;
        int errorMessage = 0;

        switch (type) {
            case Promo.TYPE_ALL:
                isValid = MobileNumberUtil.isValidNumber(mobileNumber);
                errorMessage = R.string.error_input_mobile_number_missing_0;
                break;
            case Promo.TYPE_GLOBE:
                isValid = MobileNumberUtil.isGlobeNumber(mobileNumber);
                errorMessage = R.string.error_input_mobile_number_missing_1;
                break;
            case Promo.TYPE_TM:
                isValid = MobileNumberUtil.isTMNumber(mobileNumber);
                errorMessage = R.string.error_input_mobile_number_missing_2;
                break;
            case Promo.TYPE_TATTOO:
                isValid = MobileNumberUtil.isTattooNumber(mobileNumber);
                errorMessage = R.string.error_input_mobile_number_missing_3;
                break;
        }

        if (!isValid) {
            showAlertDialog(errorMessage);
            return;
        }

        String successMessage = getString(R.string.success_send_promo_message)
                .replaceFirst(getString(R.string.success_send_promo_message_key_mobile), mobileNumber)
                .replaceFirst(getString(R.string.success_send_promo_message_key_promo), mTitle);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.success_send_load_title);
        builder.setMessage(Html.fromHtml(successMessage));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String ussdString = mUssdString.replaceFirst(getString(R.string.ussd_string_key_mobile_number), mobileNumber);

                UssdHelper.sendPromo(InputMobileNumberActivity.this, ussdString);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);

        AppCompatDialog dialog = builder.create();
        dialog.show();
    }
}
