package ph.com.globe.globeamaxretailer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import ph.com.globe.globeamaxretailer.ui.activity.ProcessTransactionsActivity;
import ph.com.globe.globeamaxretailer.util.AlarmManagerHelper;
import ph.com.globe.globeamaxretailer.util.StaticWakeLock;


public class AlarmBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Bundle extras = intent.getExtras();

        if (AlarmManagerHelper.INTENT_ACTION.equals(action) && extras != null) {
            long timeInMillis = extras.getLong(AlarmManagerHelper.INTENT_EXTRA, -1);

            if (timeInMillis > 0) {
                StaticWakeLock.lockOn(context);
                Intent activityIntent = new Intent(context, ProcessTransactionsActivity.class);
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(activityIntent);
            }
        }
    }
}
