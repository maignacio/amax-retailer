package ph.com.globe.globeamaxretailer.data;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.Calendar;
import java.util.List;

@Table(name = "Transactions", id = "_id")
public class Transaction extends Model {
    @Column(name = "schedule", notNull = true)
    public Schedule schedule;

    @Column(name = "datetime", notNull = true)
    public Calendar datetime;

    @Column(name = "recipient", notNull = true)
    public String recipient;

    @Column(name = "denomination", notNull = true)
    public int denomination;

    @Column(name = "status", notNull = true)
    public int status;

    public static Calendar createNextTransactionDatetime(Schedule schedule, Calendar calendar) {
        switch (schedule.frequency) {
            case Schedule.Frequency.DAILY:
                calendar.add(Calendar.DATE, 1);
                break;
            case Schedule.Frequency.EVERY_7_DAYS:
                calendar.add(Calendar.DATE, 7);
                break;
            case Schedule.Frequency.EVERY_15_DAYS:
                calendar.add(Calendar.DATE, 15);
                break;
            case Schedule.Frequency.EVERY_29TH:
                calendar.set(Calendar.DATE, 29);
                calendar.add(Calendar.MONTH, 1);
                break;
        }

        return calendar;
    }

    public static void createNextTransactionFromFinishedTransaction(Transaction transaction) {
        transaction.status = Status.Processed;
        transaction.save();

        Schedule schedule = transaction.schedule;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(transaction.datetime.getTimeInMillis());

        if (schedule.frequency == Schedule.Frequency.ONCE_ONLY) {
            return;
        }

        calendar = createNextTransactionDatetime(schedule, calendar);

        if (calendar.compareTo(schedule.end) > 0) {
            return;
        }

        Transaction newTransaction = new Transaction();
        newTransaction.status = Status.Pending;
        newTransaction.datetime = calendar;
        newTransaction.recipient = transaction.recipient;
        newTransaction.denomination = transaction.denomination;
        newTransaction.schedule = schedule;

        newTransaction.save();
    }

    public static List<Transaction> getTransactionsToProcess(long currentTimestamp) {
        currentTimestamp += 900000; // Add a 15 minute buffer
        return new Select().from(Transaction.class).where("datetime <= " + currentTimestamp + " AND status = " + Status.Pending).orderBy("datetime ASC").execute();
    }

    public static Transaction getTransactionToProcess(long currentTimestamp) {
        currentTimestamp += 900000; // Add a 15 minute buffer
        return new Select().from(Transaction.class).where("datetime <= " + currentTimestamp + " AND status = " + Status.Pending).orderBy("datetime ASC").executeSingle();
    }

    public static long getTimeInMillisOfNextPendingTransaction() {
        Transaction transaction = new Select().from(Transaction.class).where("status = " + Status.Pending).orderBy("datetime ASC").executeSingle();

        if (transaction != null) {
            return transaction.datetime.getTimeInMillis();
        }

        return -1;
    }

    public static void deleteTransactionsFromSchedule(long scheduleId) {
        new Delete().from(Transaction.class).where("schedule = " + scheduleId).execute();
    }

    public static void deletePendingTransactionsFromSchedule(long scheduleId) {
        new Delete().from(Transaction.class).where("schedule = " + scheduleId + " AND status = " + Status.Pending).execute();
    }

    public interface Status {
        int Pending = 0;
        int Processed = 1;
    }
}
