package ph.com.globe.globeamaxretailer.ui.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.List;

import butterknife.Bind;
import butterknife.OnItemClick;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.data.Promo;

public class PromoActivity extends BaseActivity {
    @Bind(android.R.id.list)
    ListView mListView;

    private ListAdapter mAdapter;
    private String mParentUssdString = null;

    public static void start(Activity activity) {
        start(activity, null, null);
    }

    private static void start(Activity activity, String ussdString, String title) {
        Intent intent = new Intent(activity, PromoActivity.class);

        if (!TextUtils.isEmpty(ussdString))
            intent.putExtra(Intent.EXTRA_TEXT, ussdString);

        if (!TextUtils.isEmpty(title))
            intent.putExtra(Intent.EXTRA_TITLE, title);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        enableBackButton();

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            mParentUssdString = extras.getString(Intent.EXTRA_TEXT);

            String title = extras.getString(Intent.EXTRA_TITLE);

            if (!TextUtils.isEmpty(title))
                setTitle(title);
        }

        mAdapter = new ListAdapter(this, Promo.getList(mParentUssdString));
        mListView.setAdapter(mAdapter);
    }

    @OnItemClick(android.R.id.list)
    void select(View view, int position) {
        try {
            Promo promo = (Promo) view.getTag(R.id.tag_promo);
            boolean hasChildPromo = promo.doesPromoHaveChildren();

            if (hasChildPromo) {
                // show child promo listing
                PromoActivity.start(this, promo.ussdString, promo.title);
            } else {
                // show number input screen
                InputMobileNumberActivity.start(this, promo.ussdString, promo.title);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ListAdapter extends BaseAdapter {
        private Context mContext;
        private List<Promo> mValues;

        public ListAdapter(Context context, List<Promo> values) {
            super();

            mContext = context;
            mValues = values;
        }

        @Override
        public int getCount() {
            return mValues.size();
        }

        @Override
        public Promo getItem(int position) {
            return mValues.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list, null);
            }

            Promo promo = getItem(position);

            TextView textView = (TextView) convertView;
            textView.setText(promo.title);
            textView.setCompoundDrawablesWithIntrinsicBounds(getDrawableResIdFromResourceName(promo.icon), 0, R.drawable.icons_0001_arrow, 0);

            convertView.setTag(R.id.tag_promo, promo);

            return convertView;
        }

        private int getDrawableResIdFromResourceName(String resourceName) {
            int resId = 0;

            try {
                Class res = R.drawable.class;
                Field field = res.getField(resourceName);
                resId = field.getInt(null);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return resId;
        }
    }
}
