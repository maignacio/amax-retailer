package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.tjerkw.slideexpandable.library.AbstractSlideExpandableListAdapter;
import com.tjerkw.slideexpandable.library.SlideExpandableListAdapter;

import butterknife.Bind;
import ph.com.globe.globeamaxretailer.R;
import ph.com.globe.globeamaxretailer.util.ViewHolder;

public class HelpActivity extends BaseActivity {
    @Bind(android.R.id.list)
    ListView mListView;

    private SlideExpandableListAdapter mAdapter;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, HelpActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        enableBackButton();

        mAdapter = new SlideExpandableListAdapter(
                new ListAdapter(HelpActivity.this),
                R.id.expandable_toggle_button,
                R.id.expandable
        );
        mAdapter.setItemExpandCollapseListener(new AbstractSlideExpandableListAdapter.OnItemExpandCollapseListener() {
            @Override
            public void onExpand(View itemView, int position) {
                ViewGroup parent = (ViewGroup) itemView.getParent();
                parent.setBackgroundResource(R.drawable.bg_help_selected);

                TextView title = ViewHolder.get(parent, R.id.title);
                title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_0017_help, 0, R.drawable.icons_0001_arrow_up, 0);
            }

            @Override
            public void onCollapse(View itemView, int position) {
                ViewGroup parent = (ViewGroup) itemView.getParent();
                parent.setBackgroundResource(R.drawable.bg_help);

                TextView title = ViewHolder.get(parent, R.id.title);
                title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_0017_help, 0, R.drawable.icons_0001_arrow_down, 0);
            }
        });

        mListView.addHeaderView(View.inflate(this, R.layout.header_help, null));
        mListView.addFooterView(View.inflate(this, R.layout.header_help, null));

        mListView.setAdapter(mAdapter);
    }

    private class ListAdapter extends BaseAdapter {
        private Context mContext;
        private String[] mTitles;
        private String[] mContent;

        public ListAdapter(Context context) {
            super();

            mContext = context;
            mTitles = mContext.getResources().getStringArray(R.array.titles_help);
            mContent = mContext.getResources().getStringArray(R.array.content_help);
        }

        @Override
        public int getCount() {
            return mTitles.length;
        }

        @Override
        public String getItem(int position) {
            return mTitles[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_help, null);
            }

            TextView title = ViewHolder.get(convertView, R.id.title);
            TextView content = ViewHolder.get(convertView, R.id.expandable);

            title.setText(mTitles[position]);
            content.setText(mContent[position]);

            title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_0017_help, 0, (mAdapter.getOpenPosition() == position) ? R.drawable.icons_0001_arrow_up : R.drawable.icons_0001_arrow_down, 0);
            convertView.setBackgroundResource((mAdapter.getOpenPosition() == position) ? R.drawable.bg_help_selected : R.drawable.bg_help);

            return convertView;
        }
    }
}
