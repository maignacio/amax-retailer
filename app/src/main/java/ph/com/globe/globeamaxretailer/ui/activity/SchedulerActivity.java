package ph.com.globe.globeamaxretailer.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.OnItemClick;
import ph.com.globe.globeamaxretailer.R;

public class SchedulerActivity extends BaseActivity {
    @Bind(android.R.id.list)
    ListView mListView;

    private ListAdapter mAdapter;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SchedulerActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        enableBackButton();

        mAdapter = new ListAdapter(this);
        mListView.setAdapter(mAdapter);
    }

    @OnItemClick(android.R.id.list)
    void select(int position) {
        switch (position) {
            case 0:
                SetScheduleRecipientActivity.start(SchedulerActivity.this);
                break;
            case 1:
                ManageScheduleActivity.start(SchedulerActivity.this);
                break;
            case 2:
                TransactionHistoryActivity.start(SchedulerActivity.this);
                break;
        }
    }

    private class ListAdapter extends BaseAdapter {
        private Context mContext;
        private String[] mValues;
        private TypedArray mIconDrawables;

        public ListAdapter(Context context) {
            super();

            mContext = context;
            mValues = mContext.getResources().getStringArray(R.array.values_scheduler);
            mIconDrawables = mContext.getResources().obtainTypedArray(R.array.icons_scheduler);
        }

        @Override
        public int getCount() {
            return mValues.length;
        }

        @Override
        public String getItem(int position) {
            return mValues[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list, null);
            }

            TextView textView = (TextView) convertView;
            textView.setText(getItem(position));
            textView.setCompoundDrawablesWithIntrinsicBounds(mIconDrawables.getResourceId(position, 0), 0, R.drawable.icons_0001_arrow, 0);
            return convertView;
        }
    }
}
