package ph.com.globe.globeamaxretailer.service;

import android.app.Activity;
import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.cache.ContactsCache;
import ph.com.globe.globeamaxretailer.cache.RecipientsCache;
import ph.com.globe.globeamaxretailer.data.Contact;
import ph.com.globe.globeamaxretailer.data.Schedule;
import ph.com.globe.globeamaxretailer.util.MobileNumberUtil;
import ph.com.globe.globeamaxretailer.util.PreferencesHelper;

public class ContactsService extends Service {
    private static final String PREF_FIRST_LAUNCH = "ContactService.pref_first_launch";

    private ExecutorService pool = Executors.newSingleThreadExecutor();
    private ContentObserver mObserver = new ContentObserver(new Handler()) {

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

            refreshContacts();
        }

    };

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ContactsService.class);
        activity.startService(intent);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.getContentResolver().registerContentObserver(
                ContactsContract.Contacts.CONTENT_URI, true, mObserver);

        //EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        PreferencesHelper mPrefs = PreferencesHelper.getInstance(getApplicationContext());

        if (mPrefs.getBoolean(PREF_FIRST_LAUNCH, true)) {
            refreshContacts();

            mPrefs.putBoolean(PREF_FIRST_LAUNCH, false);
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        //EventBus.getDefault().unregister(this);
        getContentResolver().unregisterContentObserver(mObserver);
        super.onDestroy();
    }

    public void refreshContacts() {
        pool.submit(new Runnable() {
            @Override
            public void run() {
                Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

                if (cursor != null) {
                    int contactId = -1;
                    String number = null;

                    Contact contact = null;

                    new Delete().from(Contact.class).execute();

                    ActiveAndroid.beginTransaction();
                    try {
                        while (cursor.moveToNext()) {
                            contactId = cursor.getInt(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                            number = MobileNumberUtil.formatNumber(cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER)));

                            if (MobileNumberUtil.isValidNumber(number)) {
                                try {
                                    contact = new Contact();
                                    contact.name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup.DISPLAY_NAME));
                                    contact.number = number;
                                    contact.uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId));

                                    contact.save();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        ActiveAndroid.setTransactionSuccessful();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        ActiveAndroid.endTransaction();
                    }

                    cursor.close();

                    RecipientsCache.clearCache();
                    ContactsCache.clearCache();
                }
            }
        });
    }
}
