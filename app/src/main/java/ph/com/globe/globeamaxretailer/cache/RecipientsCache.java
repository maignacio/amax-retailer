package ph.com.globe.globeamaxretailer.cache;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import ph.com.globe.globeamaxretailer.data.Contact;
import ph.com.globe.globeamaxretailer.data.Recipient;
import ph.com.globe.globeamaxretailer.data.Schedule;

public class RecipientsCache {
    private static SparseArray<List<Recipient>> mCache = null;
    private static LinkedList<Integer> mContactIdFetchRequests = null;
    private static ExecutorService pool = null;

    private RecipientsCache() {

    }

    private static SparseArray<List<Recipient>> getCache() {
        if (mCache == null) {
            mCache = new SparseArray<>();
        }

        if (mContactIdFetchRequests == null) {
            mContactIdFetchRequests = new LinkedList<>();
        }

        if (pool == null) {
            pool = Executors.newSingleThreadExecutor();
        }

        return mCache;
    }

    public static List<Recipient> getEntry(int key) {
        return getCache().get(key);
    }

    private static void setEntry(int key, List<Recipient> recipients) {
        getCache().put(key, recipients);
    }

    public static void clearCache() {
        getCache().clear();
        EventBus.getDefault().post(new Event());
    }

    public static void enqueueContactPhotoRequest(final Context context, int scheduleId) {
        getCache();
        synchronized (mContactIdFetchRequests) {
            mContactIdFetchRequests.add(scheduleId);
        }
        pool.submit(new Runnable() {

            @Override
            public void run() {
                synchronized (mContactIdFetchRequests) {
                    int id = -1;
                    if (!mContactIdFetchRequests.isEmpty()) {
                        id = mContactIdFetchRequests.removeFirst();

                        getContactIDFromNumber(context, id);
                    }
                }
            }
        });
    }

    private static void getContactIDFromNumber(Context context, int id) {
        try {
            List<Recipient> recipients = Schedule.getRecipients(id);
            if (recipients == null) {
                recipients = new ArrayList<>();
            }

            Contact contact = null;

            for (Recipient recipient : recipients) {
                contact = Contact.getContactFromPhone(recipient.number);

                recipient.name = (contact != null && !TextUtils.isEmpty(contact.name)) ? contact.name : recipient.number;
            }

            setEntry(id, recipients);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mContactIdFetchRequests.isEmpty()) {
            EventBus.getDefault().post(new Event());
        }
    }

    public static class Event {

    }
}