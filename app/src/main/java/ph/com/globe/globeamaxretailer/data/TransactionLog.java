package ph.com.globe.globeamaxretailer.data;

import android.database.Cursor;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Calendar;

import de.greenrobot.event.EventBus;

@Table(name = "TransactionLogs", id = "_id")
public class TransactionLog extends Model {
    @Column(name = "datetime", notNull = true)
    public Calendar datetime;

    @Column(name = "recipient", notNull = true)
    public String recipient;

    @Column(name = "denomination", notNull = true)
    public String denomination;

    @Column(name = "retailer_value", notNull = true)
    public String retailerValue;

    @Column(name = "ref_number", notNull = true)
    public String refNumber;

    public static TransactionLog addTransactionLog(Calendar datetime, String recipient, String denomination, String retailerValue, String refNumber) {
        TransactionLog log = new TransactionLog();
        log.datetime = datetime;
        log.recipient = recipient;
        log.denomination = denomination;
        log.retailerValue = retailerValue;
        log.refNumber = refNumber;

        log.save();

        EventBus.getDefault().post(new Event());

        return log;
    }

    public static Cursor getAllTransactionLogs() {
        String sql = new Select().from(TransactionLog.class).orderBy("datetime DESC").toSql();

        return ActiveAndroid.getDatabase().rawQuery(sql, null);
    }

    public static class Event {

    }
}
